
const SOCKET_READY = 1;

/**
 * Wrapper around a native websocket that manages low level details like parsing to/from json,
 * heartbeat for keepalive, etc.
 * 
 * This contains no application logic.
 * The supplied 'handler' is the starting point for all application logic about how to handle various messages.
 */
class MyWebSocketClient {
  constructor(ws, handler = (msg) => {}) {
    this.ws = ws;
    this.handler = handler;

    this._registerWebSocketHandlers(this.ws);
  }

  ready(timeoutMs = 5000) {
    return new Promise((resolve, reject) => {
      let intervalTimerId;
      let timeoutTimerId;
      intervalTimerId = setInterval(() => {
        if (this.ws.readyState === SOCKET_READY) {
          clearInterval(intervalTimerId);
          clearTimeout(timeoutTimerId);
          resolve();
        }
      }, 250);
      
      timeoutTimerId = setTimeout(() => {
        clearInterval(intervalTimerId);
        reject(new Error("timeout waiting for websocket to connect"));
      }, timeoutMs)
    })
  }

  _registerWebSocketHandlers(webSocket) {
    webSocket.onerror = (error) => {
      //console.error("ws error", error);
    }

    webSocket.onmessage = (message) => {
      //console.log("ws message", message.data);
      let parsedMessage = JSON.parse(message.data);
      this.handler(parsedMessage);
    }

    webSocket.onopen = (event) => {
      //console.log("ws opened")
    }

    webSocket.onclose = (event) => {
      //console.log("ws closed")
    }
  }

  send(message) {
    if (this.ws.readyState === SOCKET_READY) {
      this.ws.send(JSON.stringify(message))
    } else {
      console.log("could not send, socket status was " + this.ws.readyState)
      // maybe return this info to the caller?
    }
  }
}

module.exports = {
  MyWebSocketClient,
}