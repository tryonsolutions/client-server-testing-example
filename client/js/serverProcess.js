const childProcess = require('child_process')

/**
 * Encapsulates the logic for starting and stopping the backend server process.
 * 
 */
function ServerProcess() {
  var _server;
  var _port;

  this.port = () => {
    return _port;
  }

  this.startServer = () => {
    if (_server && _port) {
      return new Promise((resolve, reject) => {
        resolve({server:_server, port:_port})
      })
    } else {
      console.log('starting server for tests...')
  
      let serverPromise = new Promise((resolve, reject) => {
        _server = childProcess.spawn("sbt", ["runServer"], {cwd: "../"});
    
        _server.stdout.on('data', (data) => {
          let stringifiedData = '' + data;
          /*
            e.g. server output:
            Server online at http://localhost:55744/
            */
          if (stringifiedData.startsWith("Server online at")) {
            let port = getPortFromServerOutput(stringifiedData)
    
            console.log(`server started on port ${port}`)
            _port = port;
            resolve({server: _server, port: port});
          }
          //console.log('server stdout: ' + data)
        })
        _server.stderr.on('data', (data) => {
          console.log('server stderr: ' + data);
        });
        _server.on('close', (code) => {
          const errorMsg = 'server process existed with code ' + code
          console.log(errorMsg);
          reject(errorMsg)
        });
      });
    
      function getPortFromServerOutput(output) {
        return output.split(":")[2].substring(0, 5)
      }
    
      return serverPromise
    }
  }

  this.shutdown = () => {
    console.log('shutting down server...')
    _server.stdin.setEncoding('utf-8');
    _server.stdin.write('\n');
    _server.stdin.end();
  }
}


module.exports = ServerProcess;