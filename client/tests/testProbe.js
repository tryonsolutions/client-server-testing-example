
/**
 * Object for asynchronous testing of remote websocket.
 * 
 * Collects incoming messages so that a test can make assertions without missing messages
 * between 'send' and various assertions.
 */
class TestProbe {
  constructor() {
    this.incomingMsgQueue = [];
  }

  send(client, msg, drainMessageQueue = false) {
    if (drainMessageQueue) {
      this.incomingMsgQueue.length = 0;
    }
    
    client.handler = (msg) => {
      //console.log("test probe queuing msg",msg);
      this.incomingMsgQueue.push(msg);
    }
    client.send(msg);
  }

  drainMessageQueue(filter = () => false) {
    if (typeof filter === "function") {
      //console.log("checking filter, queue is:", this.incomingMsgQueue);
    
      let indexOfMatch = this.incomingMsgQueue.findIndex(filter);
      if (indexOfMatch > -1) {
        let msg = this.incomingMsgQueue[indexOfMatch];
        this.incomingMsgQueue.splice(0, indexOfMatch + 1);
        return msg;
      } else {
        this.incomingMsgQueue.length = 0;
        return undefined;
      }
    } else {
      throw new Error("filter was not a function")
    }
  }

  nextMsg(timeoutMs = 3000) {
    return this.fishForMsg((msg) => true, timeoutMs);
  }

  fishForMsg(filter, timeoutMs = 3000) {
    let intervalTimerId;
    let timeoutTimerId;
    return new Promise((resolve, reject) => {
      intervalTimerId = setInterval(() => {
        let msg = this.drainMessageQueue(filter)
        if (msg) {
          resolve(msg);
          clearInterval(intervalTimerId);
          clearTimeout(timeoutTimerId);
        }
      }, 250);

      timeoutTimerId = setTimeout(() => {
        clearInterval(intervalTimerId);
        reject(new Error("timed out waiting for desired message"))
      }, timeoutMs);
    });
  }
}

module.exports = {TestProbe}