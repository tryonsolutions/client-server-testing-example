const deepEqual = require('fast-deep-equal');

const expect = require('chai').expect;
const ServerProcess = require('../js/serverProcess');
const {MyWebSocketClient} = require('../js/MyWebSocketClient');
const W3CWebSocket = require('websocket').w3cwebsocket;
const {TestProbe} = require('./testProbe');

const {
  TestStatus,
  ResultStatus,
  ClientToServerMessageTypes,
  ServerToClientMessageTypes,
  ClientToServerMessages,
  ServerToClientMessages,
  ProtocolMessageContent
} = require('protocol-messages')

const sp = new ServerProcess()

/**
 * This test starts the server process before any tests,
 * and shuts it down after any tests.
 * 
 * The test is intended to test the actual javascript client code that interacts
 * with the remote server and validate that the server sends the correct
 * messages in response to the client, and that the client properly receives
 * and interprets those messages.
 */
describe('client server', function() {

  let client;
  const testProbe = new TestProbe();

  before(function(done) {
    // mocha default timeout is 2 seconds, but we need to give the server a chance to start.
    this.timeout(30000)
    sp.startServer().then(serverDetails => {
      const ws = new W3CWebSocket(`ws://localhost:${serverDetails.port}/example-websocket`)
      client = new MyWebSocketClient(ws);

      client.ready().then(() => {
        done();
      }).catch(error => {
        done(error);
      })
    }).catch(error => {
      done(error)
    })
  })

  after(() => {
    sp.shutdown()
  })

  describe('example websocket', function() {
    this.timeout(30000);

    it('should respond to my messages', async function() {
      let startMsg = ClientToServerMessages.start("1", ProtocolMessageContent.testConfig("a", "feature", ["one","two"]))
      let expectedStartResponse = ServerToClientMessages.testStarted("1", "a")

      testProbe.send(client, startMsg);
      const nextMsg = await testProbe.nextMsg();
      expect(nextMsg).to.deep.equal(expectedStartResponse);
      
      let otherStartMsg = ClientToServerMessages.start(
        "2",
        ProtocolMessageContent.testConfig(
          "b",
          "group",
          ["hi","there","person"]
        )
      )
      
      testProbe.send(client, otherStartMsg);
      testProbe.send(client, startMsg);
      // fish for the exact message
      const fishedMsg = await testProbe.fishForMsg((msg) => {
        return deepEqual(msg, expectedStartResponse)
      });
      expect(fishedMsg).to.deep.equal(expectedStartResponse);
      
      let differentMsgType = ClientToServerMessages.stop("123", "a");
      let otherStartResponse = ServerToClientMessages.testStarted("2", "b");

      testProbe.send(client, differentMsgType);
      testProbe.send(client, otherStartMsg);
      testProbe.send(client, startMsg);
      // fish for msg by a specific property
      const fishedMsg2 = await testProbe.fishForMsg((msg) => {
        return msg.msgType === ServerToClientMessageTypes.TEST_STARTED
      });
      expect(fishedMsg2).to.deep.equal(otherStartResponse);
      
    });

    it('example of failure when no next msg', async function() {
      testProbe.drainMessageQueue();
      const nextMsg = await testProbe.nextMsg();
    });

    it('example of failure when fished msg does not meet expectation', async function() {
      testProbe.drainMessageQueue();
      testProbe.send(client, ClientToServerMessages.stop("123", "a"));
      const fishedMsg = await testProbe.fishForMsg(msg => {
        return msg.msgType === ServerToClientMessageTypes.TEST_STOPPED;
      });
      expect(fishedMsg).to.deep.equal(ServerToClientMessages.testStopped("456", "b"));
    });

    it('example of failure when fishing times out', async function() {
      testProbe.drainMessageQueue();
      testProbe.send(client, ClientToServerMessages.stop("123", "a"));
      const fishedMsg = await testProbe.fishForMsg(msg => {
        return msg.msgType === ServerToClientMessageTypes.TEST_STARTED;
      });
    })
  });
});