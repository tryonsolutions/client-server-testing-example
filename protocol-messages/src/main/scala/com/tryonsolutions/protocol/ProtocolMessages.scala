package com.tryonsolutions.protocol

import play.api.libs.json._
import ProtocolMessageContent._


object TestStatus extends Enumeration {
  type TestStatus = Value
  val Pending, Running, Paused, Finished = Value

  implicit val testStatusReads = Reads.enumNameReads(TestStatus)
  implicit val testStatusWrites = Writes.enumNameWrites
}

object ResultStatus extends Enumeration {
  type ResultStatus = Value
  val Pass, Fail, True, False, Stop = Value

  implicit val resultStatusReads = Reads.enumNameReads(ResultStatus)
  implicit val resultStatusWrites = Writes.enumNameWrites
}

/**
  * Message types could be put into an Enumeration to explicitly define the set
  * of valid message types.
  */
//
//object ClientToServerMessageType extends Enumeration {
//  type ClientToServerMessageType = Value
//  val Start, Pause, Resume, Stop = Value
//
//  implicit val clientToServerMessageTypeReads = Reads.enumNameReads(ClientToServerMessageType)
//  implicit val clientToServerMessageTypeWrites = Writes.enumNameWrites
//}

/**
  * All ProtocolMessages must contain an id so we can link requests to responses,
  * and a msgType so we can do json conversion.
  *
  */
trait ProtocolMessage {
  val id: String
  val msgType: String
}

trait ClientToServerProtocolMessage extends ProtocolMessage
trait ServerToClientProtocolMessage extends ProtocolMessage

/**
  * All the valid messages the client can send to the server.
  */
object ClientToServerProtocolMessages {

  val StartType = "Start"
  case class Start(override val id: String, override val msgType: String, testConfig: TestConfig) extends ClientToServerProtocolMessage
  object Start {
    implicit val format: OFormat[Start] = Json.format[Start]
  }

  val StopType = "Stop"
  case class Stop(override val id: String, override val msgType: String, testId: String) extends ClientToServerProtocolMessage
  object Stop {
    implicit val format: OFormat[Stop] = Json.format[Stop]
  }

  def fromJson(json: JsValue): Option[ClientToServerProtocolMessage] = {
    (json \ "msgType").as[String] match {
      case StartType => json.validate[Start].asOpt
      case StopType => json.validate[Stop].asOpt
    }
  }

  def toJson(msg: ClientToServerProtocolMessage): JsValue = {
    msg match {
      case s: Start => Json.toJson(s)
      case s: Stop => Json.toJson(s)
    }
  }
}

/**
  * All the valid messages the server can send to the client.
  */
object ServerToClientProtocolMessages {

  val TestStartedType = "TestStarted"
  case class TestStarted(override val id: String, override val msgType: String = TestStartedType, testId: String) extends ServerToClientProtocolMessage
  object TestStarted {
    implicit val format: OFormat[TestStarted] = Json.format[TestStarted]
  }

  val TestStoppedType = "TestStopped"
  case class TestStopped(override val id: String, override val msgType: String = TestStoppedType, testId: String) extends ServerToClientProtocolMessage
  object TestStopped {
    implicit val format: OFormat[TestStopped] = Json.format[TestStopped]
  }

  val TestCompletedType = "TestCompleted"
  case class TestCompleted(override val id: String, override val msgType: String = TestCompletedType, testId: String) extends ServerToClientProtocolMessage
  object TestCompleted {
    implicit val format: OFormat[TestCompleted] = Json.format[TestCompleted]
  }

  val TestResultType = "TestResult"
  case class TestResult(override val id: String, override val msgType: String = TestResultType, result: ResultDetail) extends ServerToClientProtocolMessage
  object TestResult {
    implicit val format: OFormat[TestResult] = Json.format[TestResult]
  }

  val ServerErrorType = "ServerError"
  case class ServerError(override val id: String, override val msgType: String = ServerErrorType, error: ErrorDetails) extends ServerToClientProtocolMessage
  object ServerError {
    implicit val format: OFormat[ServerError] = Json.format[ServerError]
  }

  def fromJson(json: JsValue): Option[ServerToClientProtocolMessage] = {
    (json \ "msgType").as[String] match {
      case TestStartedType => json.validate[TestStarted].asOpt
      case TestStoppedType => json.validate[TestStopped].asOpt
      case TestCompletedType => json.validate[TestCompleted].asOpt
      case TestResultType => json.validate[TestResult].asOpt
      case ServerErrorType => json.validate[ServerError].asOpt
    }
  }

  def toJson(msg: ServerToClientProtocolMessage): JsValue = {
    msg match {
      case t: TestStarted => Json.toJson(t)
      case t: TestStopped => Json.toJson(t)
      case t: TestCompleted => Json.toJson(t)
      case t: TestResult => Json.toJson(t)
      case t: ServerError => Json.toJson(t)
    }
  }
}

/**
  * All objects that are not messages, but are part of messages sent from either
  * Client->Server or Server->Client.
  */
object ProtocolMessageContent {

  case class TestConfig(testId: String, testType: String, testData: Seq[String])
  object TestConfig {
    implicit val format: OFormat[TestConfig] = Json.format[TestConfig]
  }

  case class ResultDetail(testId: String, itemContent: String, status: ResultStatus.Value)
  object ResultDetail {
    implicit val format: OFormat[ResultDetail] = Json.format[ResultDetail]
  }

  case class ErrorDetails(code: Int, message: String)
  object ErrorDetails {
    implicit val format: OFormat[ErrorDetails] = Json.format[ErrorDetails]
  }

}
