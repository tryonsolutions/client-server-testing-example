package com.tryonsolutions.protocol

import org.scalatest.{FeatureSpec, Matchers}
import play.api.libs.json.{JsArray, Json}

/**
  * This spec validates that we have properly configured all our json conversion logic
  * for all our valid Protocol messages in scala.
  */
class ProtocolMessagesSpec extends FeatureSpec with Matchers {

  private val clientToServerJsonStream = this.getClass.getResourceAsStream("/clientToServerExamples.json")
  private val clientToServerJsonExamplesJson = try { Json.parse(clientToServerJsonStream) } finally { clientToServerJsonStream.close() }
  private val clientToServerExamples = clientToServerJsonExamplesJson match {
    case a: JsArray => a.value
  }

  private val serverToClientJsonStream = this.getClass.getResourceAsStream("/serverToClientExamples.json")
  private val serverToClientJsonExamplesJson = try { Json.parse(serverToClientJsonStream) } finally { serverToClientJsonStream.close() }
  private val serverToClientExamples = serverToClientJsonExamplesJson match {
    case a: JsArray => a.value
  }

  feature("i can convert client to server messages to/from json") {

    scenario("all messages") {
      clientToServerExamples.foreach { jsValue =>
        val fromJsValue = ClientToServerProtocolMessages.fromJson(jsValue)
        fromJsValue.isDefined shouldBe true
        val backToJsValue = ClientToServerProtocolMessages.toJson(fromJsValue.get)

        backToJsValue shouldBe jsValue
      }
    }
  }

  feature("i can convert server to client messages to/from json") {

    scenario("all messages") {
      serverToClientExamples.foreach { jsValue =>
        val fromJsValue = ServerToClientProtocolMessages.fromJson(jsValue)
        fromJsValue.isDefined shouldBe true
        val backToJsValue = ServerToClientProtocolMessages.toJson(fromJsValue.get)

        backToJsValue shouldBe jsValue
      }
    }
  }
}
