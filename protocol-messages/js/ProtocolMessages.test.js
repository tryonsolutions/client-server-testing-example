const expect = require('chai').expect;

const clientToServerExamples = require('../src/test/resources/clientToServerExamples.json');
const serverToClientExamples = require('../src/test/resources/serverToClientExamples.json');

/**
 * These tests aren't very interesting.
 * 
 * All this really validates is that examples are valid json...
 * 
 * It would be much more useful to...
 * - create objects with the Javascript protocol factory functions
 * - convert those to json
 * - run that json through to the scala objects
 * - convert the scala objects back to json
 * - convert that json back to the javascript protocol factory functions
 * - validate we have the same thing we started with.
 */
describe("protocol messages", function() {

  describe("client to server messages", function() {
    it("can convert to/from strings", function() {
      //console.log('cl->serv', clientToServerExamples)
      clientToServerExamples.forEach(e => {
        let toJsString = JSON.stringify(e);
        let fromJsString = JSON.parse(toJsString);
        expect(fromJsString).to.deep.equal(e);
      })
    })
  });

  describe("server to client messages", function() {
    it("can convert to/from strings", function() {
      //console.log('serv->cl', serverToClientExamples)
      serverToClientExamples.forEach(e => {
        let toJsString = JSON.stringify(e);
        let fromJsString = JSON.parse(toJsString);
        expect(fromJsString).to.deep.equal(e);
      })
    });
  });
})