# client-server-testing-example
This repository is an example architecture for explicitly defining a protocol between a client and server and a suite of tests to document the protocol usage and ensure the protocol works correctly.

## Goals
#### Separate the data structures that are shared between client and server from the client and server implementations.
Why?
- Protocol messages are defined cleanly in one place, so its easy to see what the valid messages are that a client can send to the server and vice versa.  We can easily build documentation from that so team members who rarely or never use scala/backend can still easily see what messages to send for what purpose.
It is also easy to add valid new messages, because the traits clearly define required structure for a message, and all the other messages are present as examples.

- We can make a single source of truth for those data structures - e.g. define them in scala as case classes and autogenerate the javascript implementations from that.
This makes it easy to ensure (and test) that the client and server will agree on the structure of any messages.

#### Separate the client<->server transport logic from the application logic
Why?
- There are a lot of low level details for sending data across the websocket that rarely change.  If that is all cleanly encapsulated - it reduces our mental burden.
- Most tasks are about changing application logic.  If the entry point for application logic is clearly defined and separated from low level transport details, its easier for everyone to navigate and make changes.
- We can test the client<->server protocol, with a full implementation of the server and the javascript websocket client at a layer below the desktop app.  i.e. we can make sure the javascript client has all the correct interactions with the working server independently of any presentation logic/infrastructure.


## Directory structure
`protocol-messages` contains all the messages that can be sent between the client and server, in every language that is supported.

`server` contains an implementation of the server.

`client` contains an implementation of the client, along with a suite of tests to documenting the appropriate protocol interactions between the client and server, and validating that those interactions work correctly.

note that `client` is not a UI app - its just the websocket client that interacts with the server.
One might make an app in a different module that imports and uses the already tested protocol client.
i.e. we can make as many apps as we want without having to write new protocol clients for each app.

## Commands
From the root:

`sbt protocolMessages/test` runs the scala tests to validate json conversion with scala case classes.

`sbt runServer` starts the server.

From various modules:

From `protocol-messages`,
`npm test` runs the javascript tests to validate json conversion with javascript functions.

From `client`,
`npm test` runs the javascript tests to send messages to the server and validate the server responds appropriately.