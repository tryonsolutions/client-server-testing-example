import sbt._
import Dependencies._

commands ++= Commands.all

lazy val commonSettings = Seq(
  organization := "com.tryonsolutions",
  version := "0.1.0",
  scalaVersion := "2.12.7",
  javacOptions ++= Seq("-encoding", "UTF-8")
)

lazy val protocolMessages = (project in file("protocol-messages"))
  .settings(commonSettings: _*)
  .settings(libraryDependencies ++= Seq(
    playJson,
    scalaTest
  ))

lazy val server = (project in file("server"))
  .dependsOn(protocolMessages)
  .settings(commonSettings: _*)
  .settings(libraryDependencies ++= Seq(
    akkaActor,
    akkaStream,
    akkaHttp
  ))

