package com.tryonsolutions.server

import com.tryonsolutions.protocol.ClientToServerProtocolMessages.{Start, Stop}
import com.tryonsolutions.protocol.{ClientToServerProtocolMessage, ServerToClientProtocolMessage}
import com.tryonsolutions.protocol.ServerToClientProtocolMessages.{TestStarted, TestStopped}


/**
  * Interface for handling valid messages.
  * This is the start of the application logic for how to handle various messages.
  *
  * Extend this to define our message handler.
  *
  * This assumes client->server messages are one->one or one->many.
  * i.e. Every client message must get a response - but the server may send more messages
  * as a result of processing a client message.
  */
trait ProtocolMessageHandler {
  def handle(msg: ClientToServerProtocolMessage): ServerToClientProtocolMessage
}

class ExampleProtocolMessageHandler extends ProtocolMessageHandler {
  def handle(msg: ClientToServerProtocolMessage): ServerToClientProtocolMessage = {
    msg match {
      case Start(id, msgType, testConfig) =>
        TestStarted(id, testId = testConfig.testId)
      case Stop(id, msgType, testId) =>
        TestStopped(id, testId = testId)
    }
  }
}
