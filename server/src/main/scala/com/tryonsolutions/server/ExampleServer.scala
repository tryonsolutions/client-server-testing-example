package com.tryonsolutions.server

import akka.actor.ActorSystem
import akka.http.scaladsl.Http
import akka.http.scaladsl.model.ws.{BinaryMessage, Message, TextMessage}
import akka.http.scaladsl.server.Directives._
import akka.stream.scaladsl.{Flow, Sink, Source}
import akka.stream.ActorMaterializer
import com.tryonsolutions.protocol.ProtocolMessageContent.ErrorDetails
import com.tryonsolutions.protocol.ServerToClientProtocolMessages.{ServerError, TestStarted, TestStopped}
import com.tryonsolutions.protocol._
import play.api.libs.json.Json

import scala.concurrent.{ExecutionContext, Future}
import scala.concurrent.duration.FiniteDuration
import scala.io.StdIn

/**
  * This file contains classes for handling low level details of client/server message transport.
  *
  * There is no application logic here for what to do with a given message.
  */
object ExampleServer extends App {

  start()

  def start(): Unit = {
    val DefaultPort: Int = 0

    implicit val system = ActorSystem()
    implicit val materializer = ActorMaterializer()
    implicit val executionContext = system.dispatcher

    val handler = new ExampleProtocolMessageHandler
    val handlerService = new ExampleWebSocketService(handler)
    val route =
      path("example-websocket") {
        extractUpgradeToWebSocket { upgrade =>
          complete(upgrade.handleMessages(handlerService.service))
        }
      }

    val bindingFuture = Http().bindAndHandle(route, interface = "localhost", port = DefaultPort)

    bindingFuture.foreach { binding =>
      println(s"Server online at http://localhost:${binding.localAddress.getPort}/\nPress RETURN to stop...")
    }

    StdIn.readLine() // let it run until user presses return
    bindingFuture
      .flatMap(_.unbind()) // trigger unbinding from the port
      .onComplete(_ => system.terminate()) // and shutdown when done
  }

}

class ExampleWebSocketService(val messageHandler: ProtocolMessageHandler)
                             (implicit val materializer: ActorMaterializer,
                              implicit val executionContext: ExecutionContext) {

  /**
    * Handles the low level details of converting to a Message to a valid ProtocolMessage,
    * and delegates application logic of handling the ProtocolMessage to a handler.
    */
  val service: Flow[Message, Message, Any] = Flow[Message].mapAsync[Message](1) { m =>
    m.asTextMessage.asScala.toStrict(FiniteDuration(2, "seconds")).map[Message] { msg =>
      val asJson = Json.parse(msg.text)
      val msgOpt = ClientToServerProtocolMessages.fromJson(asJson)
      if (msgOpt.isDefined) {
        val response = messageHandler.handle(msgOpt.get)
        val jsonResponse = ServerToClientProtocolMessages.toJson(response)
        TextMessage(jsonResponse.toString())
      } else {
        val response = ServerError("abc", error = ErrorDetails(1, "invalid msg"))
        val jsonResponse = ServerToClientProtocolMessages.toJson(response)
        TextMessage(jsonResponse.toString())
      }
    }
  }

}

