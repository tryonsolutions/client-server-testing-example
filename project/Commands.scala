import sbt._

object Commands {

  def runServer: Command = Command.args(name = "runServer", display = "runServer") { (state, args) =>
    val cmd = "server/runMain com.tryonsolutions.server.ExampleServer"
    state.copy(remainingCommands = state.remainingCommands :+ cmd)
  }

  val all: Seq[Command] = Seq(runServer)
}

