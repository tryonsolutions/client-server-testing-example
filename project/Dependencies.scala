import sbt._

object Dependencies {

  val akkaVersion = "2.5.17"
  val akkaHttpVersion = "10.1.5"
  val playJsonVersion = "2.6.8"
  val scalaTestVersion = "3.0.5"

  val akkaActor = "com.typesafe.akka" %% "akka-actor" % akkaVersion
  val akkaHttp = "com.typesafe.akka" %% "akka-http" % akkaHttpVersion
  val akkaStream = "com.typesafe.akka" %% "akka-stream" % akkaVersion
  val playJson = "com.typesafe.play" %% "play-json" % playJsonVersion
  val scalaTest = "org.scalatest" %% "scalatest" % scalaTestVersion % Test
}