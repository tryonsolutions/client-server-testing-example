# Todo

### protocol
- generate the javascript protocol messages from the scala representation (maybe with reflection?).

- add something to the tests to make sure that every valid protocol message is tested.

- improve the tests to include the full conversion flow - from js to json to scala to json to js - and validate the start msg = end msg.

- generate jsdoc/javadoc

### server
- can we generate javadoc for the websocket endpoint based on the 'handler'?  does that add anything beyond just javadoc on the scala case classes used in the protocol module.

### client
- can we generate jsdoc/javadoc based on the protocol tests?  does that provide any value?